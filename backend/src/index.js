const express = require('express');
const cors = require('cors');
const routes = require('./routes');

const app = express();

app.use(cors());

app.use(express.json());
app.use(routes);
/* 
 * Métodos HTTP : 
 * GET : Buscar/ Listar uma informação do back-end.
 * POST: Criar uma informação no back-end.
 * PUT: Alterar uma informação no back-end.
 * DELETE: excluir uma informação do back-end.
*/

/*
* Formas de utilizaro Banco de Dados 
* Driver : utiliza o select rotineiro ( Select * from ) 
* Query Builder : table('') 
*/
app.listen(3333);